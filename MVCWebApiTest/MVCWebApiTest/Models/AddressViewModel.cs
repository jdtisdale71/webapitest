﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebApiTest.Models
{
    public class AddressViewModel
    {
        public string Street { get; set; }
        public string Suite { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        
        public GeoViewModel Geo { get; set; }
    }

    public class GeoViewModel
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }


}