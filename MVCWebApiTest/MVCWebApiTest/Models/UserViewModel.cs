﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebApiTest.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public AddressViewModel Address { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public CompanyViewModel Company { get; set; }
        public List<UserPostsViewModel> Posts { get; set; }
    }

}