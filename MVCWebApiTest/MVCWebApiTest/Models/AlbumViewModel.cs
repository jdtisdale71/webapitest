﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWebApiTest.Models
{
    public class AlbumViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public UserViewModel User {get;set;}
        public string Title { get; set; }
        public List<AlbumPhotoViewModel> Photos { get; set; }
    }
}