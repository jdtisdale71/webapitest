﻿using MVCWebApiTest.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVCWebApiTest.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public async Task<ActionResult> Index(int id)
        {
            var user = new UserViewModel();

            using(var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://jsonplaceholder.typicode.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync($"users/{id}");

                if (response.IsSuccessStatusCode)
                {
                    var responseBody = await response.Content.ReadAsStringAsync();

                    user = JsonConvert.DeserializeObject<UserViewModel>(responseBody);
                }

                var responsePosts = await client.GetAsync($"users/{id}/posts");

                if(responsePosts.IsSuccessStatusCode)
                {
                    var responseBody = await responsePosts.Content.ReadAsStringAsync();

                    var posts = JsonConvert.DeserializeObject<List<UserPostsViewModel>>(responseBody);

                    if (user != null)
                        user.Posts = posts.OrderBy(p => p.Id).ToList();
                        
                }
            }

            return View(user);
        }
    }
}