﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using MVCWebApiTest.Models;
using Newtonsoft.Json;
using PagedList;

namespace MVCWebApiTest.Controllers
{
    public class AlbumsController : Controller
    {
        public async Task<ViewResult> Index(string currentFilter, string searchString, int? page)
        {
            if(searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IEnumerable<AlbumViewModel> albums = null;
            IEnumerable<AlbumPhotoViewModel> photos = null;
            IEnumerable<UserViewModel> users = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://jsonplaceholder.typicode.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var responseAlbums = await client.GetAsync("albums");

                if (responseAlbums.IsSuccessStatusCode)
                { 
                    var responseBody = await responseAlbums.Content.ReadAsStringAsync();

                    albums = JsonConvert.DeserializeObject<IEnumerable<AlbumViewModel>>(responseBody).OrderBy(a => a.Id);
                }

                var responseAlbumPhotos = await client.GetAsync("photos");

                if (responseAlbumPhotos.IsSuccessStatusCode)
                {
                    var responseBody = await responseAlbumPhotos.Content.ReadAsStringAsync();

                    photos = JsonConvert.DeserializeObject<IEnumerable<AlbumPhotoViewModel>>(responseBody);
                }

                var responseUsers = await client.GetAsync("users");

                if (responseUsers.IsSuccessStatusCode)
                {
                    var responseBody = await responseUsers.Content.ReadAsStringAsync();

                    users = JsonConvert.DeserializeObject<IEnumerable<UserViewModel>>(responseBody);
                }

                foreach (var album in albums.ToList())
                {
                    var albumPhotos = photos.Where(p => p.AlbumId == album.Id);

                    var photoList = new List<AlbumPhotoViewModel>();
                    foreach(var photo in albumPhotos)
                    {
                        photoList.Add(photo);
                    }
                    album.Photos = photoList;

                    album.User = users.SingleOrDefault(u => u.Id == album.UserId);
                }
            }

            if(!string.IsNullOrWhiteSpace(searchString))
            {

                albums = albums.Where(a => 
                    a.User.Name.IndexOf(searchString, StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                    a.Title.IndexOf(searchString, StringComparison.InvariantCultureIgnoreCase) >= 0
                    ).ToList();
            }

            int pageSize = 4;
            int pageNumber = (page ?? 1);
            return View(albums.ToPagedList(pageNumber,pageSize));
        }

        public async Task<ViewResult> AlbumDetails(int id)
        {
            var album = new AlbumViewModel();

            using(var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://jsonplaceholder.typicode.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync($"albums/{id}");

                if (response.IsSuccessStatusCode)
                {
                    var responseBody = await response.Content.ReadAsStringAsync();

                    album = JsonConvert.DeserializeObject<AlbumViewModel>(responseBody);
                }

                var responsePosts = await client.GetAsync($"albums/{id}/photos");

                if (responsePosts.IsSuccessStatusCode)
                {
                    var responseBody = await responsePosts.Content.ReadAsStringAsync();

                    var photos = JsonConvert.DeserializeObject<List<AlbumPhotoViewModel>>(responseBody);

                    if (album != null)
                        album.Photos = photos.OrderBy(p => p.Id).ToList();
                }
            }

            return View(album);
        }
    }
}